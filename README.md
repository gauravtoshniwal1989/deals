##Todo for scalaibility
- Persistent redis connection
- Deals page
    - Load only if the deals is active.
        - How do we check if the deal is active?
            - One way is to check it with a flag (loaded from the cache db) everytime when the page is requested. But however fast the cache might be, it still is a remote process and TCP overheads would be significant compared to the actual query execution time.
            - But deal active or not can be maintained per app process (its just an information and not a count that has to be shared by all the processes). Also the deal ending time is not significant to us, since deal is limited by number and not ending time as such (assuming that the deals would already be exhaused in a short amount of time).
            - Another is that we maintain an app variable that is loaded when the app is initialized. As long as the variable is false, we check the cache DB for the deal flag (deal is active or not). As soon as the cache flag becomes true, we modify the app variable and set it to True. As long as the app process does not restart the variable stays true and no cache is hit. This should make the check significantly fast.
    - Load only if deals are left.
        - We don't want to check the number of deals from the DB and count them everytime a deal is bought.
        - We maintain a flag of number of deals left and in the deal buy function we reduce the value of the counter (using INCRBY -1 in redis). As long as the counter is positive deals are available. This is one query in redis. **O(1)**
    - Load deals only for the correct users.
        - What are correct users?
            - Users who are logged in.
            - Users who had registered for the deal previously.
        - How to identify the correct users?
            - Logged in:
                - Get the user session (use redis as the session backend, that will make it fast). **This should be O(1)**
            - Registered for the deal
                - Get the information if the user is registered for the deal from the UserDealRegistration model. Now this is tricky, since this might be the RDS for the request.
                - ~~One way to mitigate this is to create a hash **USERDEALSTATUS|<DEAL_ID>**~~. Not sure how hash key locking is executed by the redis library. So I'll create separate keys for each UserDealRegistration with **'udr|<DEAL_ID>|<USER_ID>'** as the key and buy status as the value. If the key exists [O(1)] and the user has not bought the deal, the user should be capable to buy the deal.

- Buy deal action
	- Reduce the number of deals in the cache.
	-  Send a message in the queue that asks the consumer to modify the UserDealRegistration entry for the user and deal combination and update the deal_bought flag to True

- Future work
	- The redis instance that we will need while the deal live window (say 10 mins before and 10 mins after the deal has begun) should be a good hardware. So, to save costs, we should start this instance only in this window. For the same, we would stop setting the cache values of userdealregistered **(udr)** as a bulk operation sometime before the deal is made live (as an automated process preferably.