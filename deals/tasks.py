from celery import Celery, shared_task
import os
import sqlite3

conn = sqlite3.connect('db.sqlite3')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'deals.settings')

app = Celery('tasks', broker='pyamqp://guest@localhost//')

@shared_task
def set_user_deal_bought(user_id, deal_id):
    c = conn.cursor()
    c.execute("UPDATE mealdeal_userdealregistration SET deal_bought=1 WHERE user_id=%s AND deal_id=%s"%(user_id, deal_id))
    conn.commit()

