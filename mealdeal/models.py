from django.db import models
from .signals import save_deal_counter_in_redis
from django.dispatch import Signal
from django.db.models import signals
# Create your models here.

class Deal(models.Model):
    deal_name = models.CharField(max_length=20, null=False, blank=False)
    starts_on = models.DateTimeField(auto_now=False, auto_now_add=False)
    register_till = models.DateTimeField(auto_now=False, auto_now_add=False)
    count = models.IntegerField(null=False, blank=False)
signals.post_save.connect(save_deal_counter_in_redis, sender=Deal)

class UserDealRegistration(models.Model):
    deal = models.ForeignKey('Deal', blank=False, null=False)
    user = models.ForeignKey('userauth.User', blank=False, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    deal_bought = models.BooleanField(default=False)
