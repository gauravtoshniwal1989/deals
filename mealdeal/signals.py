from django.db.models.signals import post_save
from django.dispatch import receiver
from mealdeal import redis_connection

def save_deal_counter_in_redis(sender, instance, signal, *args, **kwargs):
    print('dealsleft|%d'%(instance.pk))
    redis_connection.set('dealsleft|%d'%(instance.pk), instance.count)
