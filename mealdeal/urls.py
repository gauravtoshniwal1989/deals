from django.conf.urls import url

from .views import *

app_name = "deal"
urlpatterns = [
        url(r'^register/(?P<deal_id>[0-9]+)/$', DealRegistrationView.as_view(), name='dealregistration'),
        url(r'^registration/success/(?P<deal_id>[0-9]+)/$', DealRegistrationSuccessfulView.as_view(), name='registrationsuccessful'),
        url(r'^buy/(?P<deal_id>[0-9]+)/$', DealBuyView.as_view(), name='buy'),
        url(r'^buy/success/$', BuySuccessView.as_view(), name='buysuccess'),
]
