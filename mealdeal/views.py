from django.shortcuts import render, get_object_or_404 
from django.contrib.auth.decorators import login_required
from mealdeal.models import Deal, UserDealRegistration
from django.views import View
from django.views.generic.base import TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.apps import apps
from mealdeal import redis_connection
from django.core.cache import caches
from django.utils import timezone
from deals.tasks import set_user_deal_bought

inmem_cache = caches['inmem']
cache = caches['default']


class DealRegistrationView(View):

    def dispatch(self, request, *args, **kwargs):
        self.deal_id = kwargs.get('deal_id', None)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        set_user_deal_bought.delay(request.user.id, self.deal_id)
        deal =  get_object_or_404(Deal, pk=self.deal_id)
        return render(request, 'deal.html',
                    {'deal':deal}
                )

    def register_user_for_deal(self, user, deal):
        # Its not a problem updating the deal registration directly to DB because these requests would not come in a burst.
        # So we'll stick to the ususal DB query as well
        userdealregistration, created = UserDealRegistration.objects.get_or_create(user=user, deal=deal)
        if created:
            redis_connection.set('udr|%d|%d'%(deal.id, user.id), 0)
        return userdealregistration

    def post(self, request, *args, **kwargs):
        deal = get_object_or_404(Deal, pk=self.deal_id)
        userdealregistration = self.register_user_for_deal(request.user, deal)
        return HttpResponseRedirect(reverse('deal:registrationsuccessful', kwargs={'deal_id':deal.id}))


class DealRegistrationSuccessfulView(View):

    def dispatch(self, request, *args, **kwargs):
        self.deal_id = kwargs.get('deal_id', None)
        return super().dispatch(request, *args, **kwargs)
   
    def get(self, request, *args, **kwargs):
        deal =  get_object_or_404(Deal, pk=self.deal_id)
        return render(request, 'registrationsuccessful.html',
                    {'deal':deal}
                )


class DealBuyView(View):
    
    def set_deal_bought(self, user_id, deal_id):
        '''
        Model operation to be executed in the DB for this step is:
        >>deal_registration = get_object_or_404(UserDealRegistration, user__id=user_id, deal__id=deal_id)
        >>if deal_registration.deal_bought is False:
        >>    deal_registration.deal_bought=True
        >>    deal_registration.save()
        However, we'll now update the DB using an async process
        '''
        redis_connection.set('udr|%d|%d'%(deal_id, user_id), 1)
        redis_connection.incrby('dealsleft|%d'%(deal_id),-1)
        # TODO: Produce the task for the consumer and put it in the queue
        # The consumer will update the UserDealRegistration object in DB and set the udr key val 1
        set_user_deal_bought.delay(user_id, deal_id)

    def is_deal_available(self, deal_id):
        '''
        This is equivalent to the following, but to not issue count() for every request, we get the count from cache
        deals_bought_count = UserDealRegistration.objects.filter(deal=deal, deal_bought=True).count()
        '''
        num_deals_available = redis_connection.get('dealsleft|%d'%(deal_id))
        if num_deals_available and int(num_deals_available)>0:
            return True
        return False

    def get_deal(self, deal_id):
        '''
        Get the deal object from memory, because we don't want to query the DB for each request.
        We get the deal instance from DB if its not already in local memory cache
        and save it in the localmem cache. That way the deal instance will be quickly available whenver needed
        '''
        deal = inmem_cache.get('deal|%s'%(deal_id))
        if deal is None:
            deal = get_object_or_404(Deal, pk=deal_id)
            inmem_cache.set('deal|%s'%(deal_id), deal)
        return deal

    def is_deal_live(self, deal_id):
        '''
        Get the deal instance from cache and compare the start time with current datetime
        '''
        deal = inmem_cache.get('deal|%d'%(deal_id))
        if deal and timezone.now() > deal.starts_on:
            return True
        return False

    def is_customer_registered_for_deal(self, deal, user):
        '''
        The equivalent non optimized version of the function is as below:
        try:
            deal_registration = UserDealRegistration.objects.get(deal=deal, user=user)
            return True
        except UserDealRegistration.DoesNotExist:
            return False
        '''
        user_deal_registration = redis_connection.get('udr|%d|%d'%(deal.id, user.id))
        if user_deal_registration is not None and int(user_deal_registration)==0:
            return True
        return False


    def dispatch(self, request, *args, **kwargs):
        self.deal_id = kwargs.get('deal_id', None)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        deal = self.get_deal(kwargs.get('deal_id'))
        if not self.is_deal_live(deal.id):
            return render(request, 'buy.html', {'deal':None, 'status':'The deal is not live yet. Come back in some time.'})
        if self.is_deal_available(deal.id):
            if self.is_customer_registered_for_deal(deal, request.user):
                return render(request, 'buy.html', {'deal':deal, 'status':''})
            else:
                # TODO: Cover the case where a user has already bought the deal
                return render(request, 'buy.html', {'deal':None, 'status':'Oops! You are not registered for the deal. Maybe register the next time'})
        else: # Inventory not available
            return render(request, 'buy.html', {'deal':None, 'status':'Sorry, we are out of food'})

    def post(self, request, *args, **kwargs):
        deal = self.get_deal(kwargs.get('deal_id'))
        if self.is_deal_live(deal.id) and self.is_deal_available(deal.id):
            self.set_deal_bought(request.user.id, deal.id)
            return HttpResponseRedirect(reverse('deal:buysuccess'))
        else: # Inventory not available
            return render(request, 'buy.html', {'deal':None, 'status':'Sorry, we are out of food'})


class BuySuccessView(TemplateView):
    template_name = 'buysuccess.html'
