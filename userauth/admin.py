from django.contrib import admin
from .models import *
from django import forms
# Register your models here.

class SwiggyUserAdminForm(forms.ModelForm):
    class Meta:
        model = User
        fields = "__all__"
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password'].widget = forms.PasswordInput()

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class SwiggyUserAdmin(admin.ModelAdmin):
    form = SwiggyUserAdminForm

admin.site.register(User, SwiggyUserAdmin)

